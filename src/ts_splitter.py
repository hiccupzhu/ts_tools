#!/usr/bin/env python
#coding=utf-8

__version__ = "0.1.0"

import os
import sys

def usage():
    usage_info = """\
Usage Info:
    matrix_plot <options> [matrix_filename]
    options:
        -h | --help:    Get help infomation.
        -v | --version: Get tool's current version.
        -i : Set input filename
        -s | --skip: skip num bytes then start to read
        -c : splite count
    eg.
        ts_splitter -i /opt/video_pts.txt
""";
    print usage_info;

def dump_file(dfilename, ifp, size):
    print 'Create "%s" ...' % (dfilename)
    ofp = open(dfilename, 'wb');
    sz = 0;
    while sz < size:
        rnum = 0;
        remained = size - sz;
        if remained >= 4096:
            rnum = 4096;
        else:
            rnum = remained;
            
        data = ifp.read(rnum);
        sz += len(data);
        ofp.write(data);
    ofp.close();
    print 'Dump "%s" done.' % (dfilename)

def ts_splite_file(origin_filename, counter):
    size = os.path.getsize(origin_filename);
    
    peer_size = [];
    peer = (size / counter) - (size / counter) % 188;
    for i in range(0, counter):
        if i == counter - 1:
            peer_size.append(size - sum(peer_size))
        else:
            peer_size.append(peer);
    
    name, ext = os.path.splitext(origin_filename);
    
    ifp = open(origin_filename, 'r');
    for i in range(0, counter):
        dump_file('%s_%03d.%s' % (name, i, ext[1:]), ifp, peer_size[i]);
    
    ifp.close();
    

if __name__ == "__main__":
    i = 1;
    count = 0;
    filename = "";
    
    while i < len(sys.argv):
        opt = sys.argv[i];
        
        if opt in ('-h', '--help'):
            usage();
            sys.exit(0);
        elif opt in ('-v', '--version'):
            print 'Current version:%s' % (__version__);
            sys.exit(0);
        elif opt in ('-c'):
            i += 1;
            count = long(sys.argv[i]);
        elif opt in ('-i'):
            i += 1;
            filename = sys.argv[i];
        else:
            print 'Unknown args: "%s"' % (sys.argv[i]);
        
        i += 1;
    
    if filename == "":
        print 'Please set splite filename with: -i';
        sys.exit(0);
    
    if os.path.exists(filename) == False:
        print 'File:"%s" NOT exist, pls check it.' % (filename);
        sys.exit(0);
    
    ts_splite_file(filename, count);
        