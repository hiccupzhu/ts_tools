#!/usr/bin/env python
#coding=utf-8

__version__ = "0.1.0"

import threading
import time
import os
import subprocess
import re
import sys
import getopt
import traceback
import logging
from signal import signal, SIGPIPE, SIG_DFL


version = __version__
is_check_video = False;
is_check_audio = False;
g_uri = "";
INVALID_TIME = -1L;

logger = logging.getLogger("tsreport_application");
logger.setLevel(logging.DEBUG);


class TimeStamp:
    name = "";
    ptss = [];
    dtss = [];
    def __init__(self, name):
        self.name = name;
        self.pts = INVALID_TIME;
        self.dts = INVALID_TIME;
        self.last_pts = INVALID_TIME;
        self.last_dts = INVALID_TIME;
        
    def set_pts_dts(self, pts, dts):
        if(pts != INVALID_TIME):
            self.last_pts = self.pts;
            self.pts = pts;
            self.ptss.append(pts);
        if(dts != INVALID_TIME):
            self.last_dts = self.dts;
            self.dts = dts;
            self.dtss.append(dts);

    def check_pts_dts(self):
        pts = self.pts;
        dts = self.dts;
        last_pts = self.last_pts;
        last_dts = self.last_dts;
        if(pts != INVALID_TIME and last_pts >= pts):
            print "%s pts ERROR [PTS]%d [LAST_PTS]%d =%d" %(self.name, pts, last_pts, pts - last_pts)
        if(dts != INVALID_TIME and last_dts >= dts):
            print "%s dts ERROR [DTS]%d [LAST_DTS]%d =%d" %(self.name, dts, last_dts, dts - last_dts)

    def save_array(self, filename, tss):
        fp = open(filename, "w");
        for ts in tss:
            fp.write("%d " % (ts));
        fp.close();

    def save_pts(self, filename = ""):
        if( filename == ""):
            filename = "%s_pts.txt" % (self.name);
        self.save_array(filename, self.ptss);

    def save_dts(self, filename = ""):
        if( filename == ""):
            filename = "%s_pts.txt" % (self.name);
        self.save_array(filename, self.dtss);

##vt = TimeStamp("Video");
##at = TimeStamp("Audio");



def usage():
    cmd = """
    tsreport.py [options]
         -h|--help: Get help infomation.
         -v|--version: Get tool current version.
         --uri: Set file path.
         --video: check video PTS
         --audio: chech audio PTS
    eg.
        ./tsreport.py --audio --uri=/opt/aaaaa/800_1377654098_22001687.ts
    """;
    print cmd;

def transform_time(t, unit = "ns"):
    ret = 0;
    if unit == "ms":
        ret = t / 90;
    elif unit == "us":
        ret = t / 90 * 1000;
    elif unit == "ns":
        ret = t / 90 * 1000000;
    else:
        ret = t;
    return ret;

def get_info_from_text(info):
    info = info.replace("\n", " ");
    #
    # "position packet-count PID PTS DTS type PCR"
    #
    msg = ();

    m = re.match('^ +([0-9]+): TS Packet ([0-9]+) PID ([0-9a-f]{4}) \[pusi\]', info);
    if m:
        msg += m.groups();

    m = re.search('PTS ([0-9]+)', info);
    if m:
        pts = transform_time(long(m.group(1)));
        msg += (pts,);
    else:
        msg += (-1,);

    m = re.search('DTS ([0-9]+)', info);
    if m:
        dts = transform_time(long(m.group(1)));
        msg += (dts,);
    else:
        msg += (-1,);

    m = re.search('Video stream', info);
    if m:
        msg += ("V",);

    m = re.search('Audio stream', info);
    if m:
        msg += ("A",);

    m = re.search('\[pusi\] PMT', info);
    if m:
        msg += ("PMT",);

    m = re.search('\[pusi\] PAT', info);
    if m:
        msg += ("PAT",);

    m = re.search('PCR ([0-9]+)', info);
    if m:
        pcr = long(m.group(1));
        pcr = pcr / 27 * 1000
        msg += (pcr,);
    
    return msg;


if __name__ == "__main__":
    if (len(sys.argv) < 2):
        print "Too few args."
        exit(0);

    signal(SIGPIPE,SIG_DFL)
    
    opts, args = getopt.getopt(sys.argv[1:],'vh', ['help', 'version', 'uri=', 'video', 'audio']);
    for key,value in opts:
        if key in ("-v", "--version"):
            print "Current Version:", version;
            exit(0);
        elif key in ("-h", "--help" ):
            usage();
            exit(0);
        elif key in ("--uri"):
            g_uri = value;
        elif key in ("--video"):
            is_check_video = True;
        elif key in ("--audio"):
            is_check_audio = True;
        else:
            print "UNKONOWN args:" + str(key);
    
    cmd = '''tsreport -v %s ''' % (g_uri)
    print "cmd=", cmd;
    pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True);

    print "###", os.path.splitext(g_uri)[0];

    fh = logging.FileHandler(os.path.splitext(g_uri)[0] + ".log", mode = 'w')
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
##    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
##    fh.setFormatter(formatter)
##    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    print "****************************************"
    print "position packet-count PID PTS DTS type PCR"
    info = "";
    count = 0;
    while True:
        try:
            line = pipe.stdout.readline();
            if re.search('^ +[0-9]+: TS Packet [0-9]+ PID [0-9a-f]{4} \[pusi\]', line):
                if info != "":
                    msg = get_info_from_text(info)[0:];
                    for p in ("'", "(", ")", ",", "L"):
                        msg = str(msg).replace(p, "");
                    logger.info(msg);
                    
                    info = "";
                
                info += line;
            else:
                info += line;
        except:
            print "popen ERROR!!"
            traceback.print_exc();
            break;
        if pipe.poll() != None:
            break;
    print "****************************************"

    pipe.poll();



        
