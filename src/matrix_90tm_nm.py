#!/usr/bin/env python
#coding=utf-8

__version__ = "0.1.0"

import os
import sys


def usage():
    usage_info = """\
Usage Info:
    src.matrix_90tm_nm <options> [matrix_filename]
    options:
        -h | --help:    Get help infomation.
        -v | --version: Get tool's current version.
        -i : Set input filename
    eg.
        matrix_90tm_nm -i /opt/video_pts.txt
    
    NOTICE:
        Unit is 'nm'
""";
    print usage_info;


if __name__ == '__main__':
    i = 1;
    filename = "";
    while i < len(sys.argv):
        opts = sys.argv[i];
        if opts in ('-h', '--help'):
            usage();
            sys.exit(0);
        elif opts in ('-v', '--version'):
            print('Current Version:%s' % (__version__));
            sys.exit(0);
        elif opts in ('-i'):
            i += 1;
            filename = sys.argv[i];
        else:
            print 'Unknown args: "%s"' % (sys.argv[i]);
        
        i += 1;
    
    if filename == "":
        print('Please set splite filename with: "-i"');
        sys.exit(0);
    
    if os.path.exists(filename) == False:
        print('File:"%s" NOT exist, pls check it.' % (filename));
        sys.exit(0);
    
    
    X = [];
    for line in file(filename, 'r'):
        if line.strip() == "":
            continue;
        for word in line.split():
            X.append(long(word));
    
    for x in X:
        print ('%d' % (x/90*1000000));