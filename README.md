# Introduction
ts_tools is a set to analyse TS stream

***

# Install from source

## Prerequisites

python >= 2.6


***
## Tools Description

###ts_splitter.py:
To splite one TS file with 188 alignment.

###tsreport.py:
It is based on the [tstools](https://code.google.com/p/tstools/), one of them 'tsreport -v' output.
The script analyse the output log to build the PID map.

###matrix_90tm_nm.py:
It is reading a file which is contain lots of numbers(ASCII) separated by a space or '\n';
Then M = (M / 90 * 1000000), the unit is 'nm'. 

